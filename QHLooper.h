#ifndef QH_LOOPER_H
#define QH_LOOPER_H

#include <Messenger.h>
#include <Font.h>
#include <Looper.h>
//#include 

class QHMethod;
class QHStatusWindow;
class BMenu;
class BMessenger;

extern Preferences gSettings;

class QHLooper : public BLooper
{
public:
						QHLooper(QHMethod* method);
	virtual	void		Quit();
	virtual void		MessageReceived(BMessage* msg);
	
	 		void		EnqueueMessage(BMessage* msg);
	 		status_t	Init();
	 		status_t	ReadSettings(char* basePath);
	 		
	 		void		SendInputStopped();
	 		void		SendInputStarted();
private:
			void		_HandleKeyDown(BMessage* msg);
			void		_HandleLocationReply(BMessage* msg);
			void		_HandleMethodActivated(bool active);
			void		_ProcessResult(uint32 result);
			void		_ForceXXXX();
			
			QHMethod*	fowner;
			// need something
			
			BFont		f
			
}


#endif
