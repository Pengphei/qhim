
// QH method

#include <List.h>
#include <Looper.h>
#include <FindDirectory.h>
#include <Path.h>
#include <File.h>
#include <Screen.h>

#include "IconImages.h"
#include "QHCommon.h"
#include "QHMethod.h"

Preferences gSettings;

BInputServerMethod*
instantiate_input_method()
{
	return new QHMethod();
}


QHMethod::QHMethod()
	:	BInputServerMethod("QHIM", (const uchar*)kQHIMIcon)
{
	QH_DEBUG_PRINT("QHIM: Constructor called.\n");
	ReadSettings();
}

QHMethod::~QHMethod()
{
	BLooper* looper = NULL;
	qhLooper.Target(&looper);
	
	if(looper != NULL)
	{
		QH_DEBUG_PRINT("QHIM: Locking QHLooper...\n");
		if(looper->Lock())
			QH_DEBUG_PRINT("QHIM: QHLooper locked. Calling Quit().\n");
		looper->Quit();
	}
	WriteSettings();
}

status_t
QHMethod::MethodActivated(bool active)
{
	BMessage msg(QH_METHOD_ACTIVATED);
	
	if(active)
		msg.AddBool("active", true);
	
	qhLooper.SendMessage(&msg);
	
	return B_OK;
}

filter_result
QHMethod::Filter(BMessage* msg, BList* outList)
{
	if(msg->what != B_KEY_DOWN)
		return B_DISPATCH_MESSAGE;
	
	qhlooper.SendMessage(msg);
	return B_SKIP_MESSAGE;
}

status_t
QHMethod::InitCheck()
{
	QH_DEBUG_PRINT("QHIM: InitCheck() called.\n");
	
	QHLooper* looper;
	status_t error;
	looper = new QHLooper(this);
	looper->Lock();
	error = looper->Init();
	looper->Unlock();
	qhLooper = BMessenger(NULL, looper);
	
	if(error != B_NO_ERROR)
		QH_DEBUG_PRINT("QHLooper::InitCheck() failed.\n");
	else
		QH_DEBUG_PRINT("QHLooper::InitCheck() success.\n");
	
	return err;
}

void
QHMethod::ReadSettings()
{
	BMessage pref;
	BFile preffile(QHIM_SETTINGS_FILE, B_READ_ONLY);
	if(preffile.InitCheck() == B_NO_ERROR 
		&& prefile.Unflatten(&preffile) == B_OK)
	{
		pref.FindBool("arrowkey", &gSettings.convert_arrowkey);
		pref.FindPoint("statuswindow", &gSettings.SW_location);
		QH_DEBUG_PRINT("QHIM: ReadSettings() success. arrowkey=%d, SW_location=(%d, %d).\n",
				gSettings.convert_arrowkey, gSettings.SW_location.x, gSettings.SW_location.y);
		
		return;
	}
	// Default Settings
	QH_DEBUG_PRINT("QHIM: ReadSettings() failed.\n");
	gSettings.convert_arrowkey = true;
	
	// fix me? set sw window according to screen resolution
	gSettings.SW_location = (800, 720);
	
}

void
QHMethod::WriteSetting()
{
	BMessage pref;
	BFile preffile(QHIM_SETTINGS_FILE, 
			B_WRITE_ONLY | B_CREATE_FILE | B_ERASE_FILE);
	
	if(preffile.InitCheck() == B_NO_ERROR)
	{
		pref.AddBool("arrowkey", gSettings.convert_arrowkey);
		pref/AddPoint("statuswindow", gSettings.SW_location);
		
		pref.Flatten(&preffile);
		
		QH_DEBUG_PRINT("QHIM: WriteSettings() success. arrowkey=%d, SW_location=(%d, %d)",
				gSettings.convert_arrowkey, gSettings.SW_location.x, gSettings.SW_location.y);
	}	
}









