
#include "QHStatusWindow.h"
#include "IconImages.h"

#include <Bitmap.h>
#include <Screen.h>

#include "WindowPrivate.h"

QHStatusWindow::QHStatusWindow(BRect rect, BLooper* looper)
	:	BWindow(rect, B_EMPTY_STRING, kLeftTitleWindowLook,
				B_FLOATING_ALL_WINDOW_FEEL,
				B_NOT_RESIZABLE | B_NOT_ZOOMABLE | B_NOT_CLOSABLE |
				B_AVOID_FOCUS | B_WILL_ACCEPT_FIRST_CLICK)
{
	
	// draw background, logoButton, langButton,
	// HFWSButton, PunctuationButton, SoftkeyboardButton,
	// VoiceButton, and ConfigButton.
	ResizeTo(122, 46);
	BRect frame = Frame();
	frame.OffsetTo(-1, -1);
	frame.bottom += 3;
	frame.right += 3;
	back = new BBox(frame);
	AddChild(back);
	
	BRect iconrect(0, 0, Iconwidth, Iconheight);
	
	BPicture *onpict, *offpic;
	BBitmap  *onimage, *offimage;
	
	back->MovePenTo(0, 0);
	
	//....
	
	delete onpict;
	delete offpict;
	delete onimage;
	delete offimage;
	
}


void 
QHStatusWindow::MessageReceived(BMessage* msg)
{
	int32 mode;
	switch(msg->what)
	{
		case STATUS_WINDOW_HIDE:
		{
			if(!IsHidden())
				Hide();
			break;
		}
		case STATUS_WINDOW_SHOW:
		{
			if(IsHidden())
			{
				BRect frame = Frame();
				BRect screenrect = BScreen().Frame();
				float x, y;
				x = screenrect.right - frame.right;
				y = screenrect.bottom - frame.bottom;
				
				if(x < 0)
					frame.OffsetBy(x, 0);
				if(y < 0)
					frame.OffsetBy(0, y);
				
				MoveTo(frame.left, frame.top);
				SetWorkspaces(B_CURRENT_WORKSPACE);
				Show();
			}
			break;
		}
		case STATUS_WINDOW_MODE_CHANGE:
		{
			break;
		}
		case STATUS_WINDOW_BUTT_UPDATE:
		{
			break;
		}
		default:
		{
			BWindow::MessageReceived(msg);
		}
			
		
	}
}

void 
StatusWindow::AllButtonOff()
{
	LangButton->SetValue(B_CONTROL_OFF);
	HFWSButton->SetValue(B_CONTROL_OFF);
	PunctuationButton->SetValue(B_CONTROL_OFF);
	SoftKeyboardButon->SetValue(B_CONTROL_OFF);
	VoiceButton->SetValue(B_CONTROL_OFF);
	ConfigButton->SetValue(B_CONTROL_OFF);
}

void
StatusWindow::FrameMoved(BPoint screenPoint)
{
	gSettings.SW_location = screenPoint;
}


