/*
 * this is QH input method.
 */
 
#ifndef QH_METHOD_H
#define QH_METHOD_H

#include <Messenger.h>
#include <InputServerMethod.h>

extern "C" _EXPORT BInputServerMethod* instantiate_input_method();

class QHLooper;

class QHMethod : public BInputServerMethod
{
public:
						QHMethod();
	virtual				~QHMethod();
	status_t			MethodActivated(bool active);
	status_t			InitCheck();
	filter_result		Filter(BMessage* message, BList *outList);

private:
	BMessenger			qhLooper;
	void				ReadSetting();
	void				WriteSetting();
}


#endif
