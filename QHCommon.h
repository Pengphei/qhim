#ifndef	QH_COMMON_H
#define QH_COMMON_H

#include <SupportDefs.h>
#include <Point.h>

// BMessage constant
#define QH_IM_ACTIVATED					"Qact"


// Input Window messages
#define INPUT_WINDOW_SHOW
#define INPUT_WINDOW_HIDE
#define INPUT_WINDOW_SHOWAT
#define INPUT_WINDOW_SETTXT
#define INPUT_WINDOW_SELECT
#define INPUT_WINDOW_


// QH Status window messages
#define STATUS_WINDOW_SHOW				"Sshw"
#define STATUS_WINDOW_HDIE				"Shid"
#define STATUS_WINDOW_BUTT_UPDATE		"Supd"
#define STATUS_WINDOW_MODE_CHANGE		"Schg"


#define QH_SETTINGS_FILE				"/boot/home/config/settings/qinhan_settings"

typedef struct
{
	BPoint 	SW_location;
	bool	convert_arrowkey;
	
} Preferences;


#endif //END OF QH_COMMON_H
