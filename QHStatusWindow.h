/*
	this is the UI for QH
	UI is separated for four parts:
		1. icon stayed in Deskbar
		2. state window always shown,could be configred
		3. input window only shown when typing in
		4. configure window 
	
*/


#ifndef QH_STATUS_WINDOW_H
#define QH_STATUS_WINDOW_H


#include "QHCommon.h"
#include <Window.h>
#include <PictureButton.h>
#include <Box.h>

extern Preferences gSettings;

class QHStatusWindow : public BWindow
{
private:
	BBox*				Background;
	BPictureButton*		LogoButton;
	BPictureButton*		LangButton;			//language button
	BPictureButton*		HFWSButton;			//Half and Full Width switch button
	BPictureButton*		PunctuationButton;	//
	BPictureButton*		SoftKeyboardButon;
	BPictureButton*		VoiceButton;		//Voice input Button
	BPictureButton*		ConfigButton;		//Configure Button
	
	void 				AllButtonOff();

public:
						QHStatusWindow(BRect rect, BLooper* looper);
	virtual void		MessageReceived(BMessage* msg);
	virtual void		FrameMoved(BPoint screenPoint);
	
}

#endif
