
#include "QHIM.h"
#include "QHStatusWindow.h"

#include <Alert.h>
#include <Application.h>
#include <Deskbar.h>
#include <Entry.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



#undef B_TRANSLATE_CONTEXT
#define B_TRANSLATE_CONTEXT "QHIM"

class QHIM : public BApplication
{
public:
						QHIM();
	virtual				~QHIM();
	
	virtual void		ReadytoRun();
	virtual void		AboutRequested();
	
	
};

const char* kAppSig = "application/x-vnd.Haiku-QHInputMethod";
const char* kDeskbarSig = "application/x-vnd.Be-TSKB";
const char* kDeskbarItemName = "QHIM";

status_t 
our_image(image_info& image)
{
	int32 cookie = 0;
	while(get_next_image_info(B_CURRENT_TEAM, &cookie, &image) == B_OK)
	{
		if ((char*)our_image >= (char*)image.text
			&& (char*)our_image <= (char*)image.text + image.text_size)
			return B_OK;
	}
	
	return B_ERROR;
}

// #pragma mark -

QHIM::QHIM()
	:	BApplication(kAppSig)
{
}

QHIM::~QHIM()
{
}

void
QHIM::ReadyToRun()
{
	bool isDeskbarRunning = true;
	bool isInstalled = false;
	

	
	
	// first time launch QHIM, it will check boot script, and
	// ask user to add itself to boot script.
	
	/*
		BAlert* alert = new BAlert("",
			B_TRANSLATE("This is your first time to run QHIM input method, You could addd it " 
			"to the boot script, every time Haiku boots, it could be launched automatically."),
			B_TRANSLATE("Add to boot script"), NULL, NULL,  B_WIDTH_AS_USUAL,
			B_WARNING_ALERT);
			alert->SetShortcut(0, B_ESCAPE);
	*/
	
	// check whether QHIM has stayed in Deskbar and
	// whether Deskbar is running. 
	// these APIs is unique for Haiku.
	BDeskbar deskbar;
	isDeskbarRunning = deskbar.IsRunning();
	isInstalled = deskbar.HasItem(kDeskbarItemName);
	
	if (isDeskbarRunning && !isInstalled)
	{
		BAlert* alert = new BAlert("",
			BTRANSLATE("You can run QHIM in a window " "or install QH to Deskbar.",
			BTRANSLATE("Run in window"), BTRANSLATE("Install in Deskbar"), NULL, B_WIDTH_AS_USUAL,
			B_WARNING_ALERT);
		alert->SetShortcut(0, B_ESCAPE);
		
		if(alert->Go())
		{
			image_info info;
			entry_ref ref;
			
			if(our_image(info) == B_OK 
				&& get_ref_for_path(info.name, &ref) == B_OK)
			{
				BDeskbar deskbar;
				deskbar.AddItem(&ref);
			}
			
			Quit();
			return;
		}
	}
	BWindow* window = new QHStatusWindow();
	window->Show();
}

void
QHIM::AboutRequested()
{
	BWindow* window = WindowAt(0);
	if (window == NULL)
		return;
	
	BView* view = window->FindView(kDeskbarItemName);
	if (view == NULL)
		return;
	
	BMessenger target((BHandler*)view);
	BMessage about(B_ABOUT_REQUESTED);
	targete.SendMessage(&about);
	
}



//	#pragma mark -
